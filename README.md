# Malaz

Malaz is a simple revese-proxy

Build with
`MIX_ENV=prod mix release`

---

Env vars:
 - PORT: the port the app listens to, e.g `80`
 - UPSTREAMS: list of upstreams the app should handle, e.g.
 `foo.bar -> 127.0.0.1:4044, foo.baz -> 127.0.0.1.4040`
