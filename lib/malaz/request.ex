defmodule Malaz.Request do
  @moduledoc false

  alias Malaz.Token
  alias Plug.Conn

  @spec build(Token.t()) :: Token.t()
  def build(token) do
    token
    |> update_header()
    |> extract_body()
    |> parse_method()
    |> IO.inspect()
  end

  @spec update_header(Token.t()) :: Token.t()
  defp update_header(%Token{conn: conn} = token) do
    ip_addr = conn.remote_ip |> :inet.ntoa() |> to_string

    conn =
      conn
      |> Conn.put_req_header("x-forwarded-for", ip_addr)
      |> Conn.delete_req_header("host")
      |> Conn.delete_req_header("transfer-encoding")

    %{token | conn: conn}
  end

  @spec extract_body(Token.t()) :: Token.t()
  defp extract_body(%Token{conn: conn} = token) do
    {:ok, body, _conn} = Conn.read_body(conn)
    %{token | body: body}
  end

  @spec parse_method(Token.t()) :: Token.t()
  defp parse_method(%Token{conn: conn} = token) do
    method = conn.method |> String.downcase() |> String.to_atom()
    %{token | method: method}
  end
end
