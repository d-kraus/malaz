defmodule Malaz.RoutingTable do
  @moduledoc false

  use Agent

  def start_link(_) do
    upstreams =
      Application.get_env(:malaz, :upstreams, %{})
      |> String.split(",")
      |> Enum.map(&String.split(&1, "->"))
      |> List.flatten()
      |> Enum.map(&String.trim(&1))
      |> Enum.map(&String.trim_leading(&1))
      |> Enum.chunk_every(2)
      |> Enum.map(&List.to_tuple(&1))
      |> Map.new()

    Agent.start_link(fn -> upstreams end, name: __MODULE__)
  end

  def get_route(host) do
    __MODULE__
    |> Agent.get(& &1)
    |> Map.get(host)
  end
end
