defmodule Malaz.Token do
  @moduledoc false

  defstruct conn: nil,
            body: nil,
            target: nil,
            method: nil
end
