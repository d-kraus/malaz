defmodule Malaz.Router do
  @moduledoc false

  alias Malaz.RoutingTable
  alias Malaz.Token

  def match(%Token{conn: conn} = token) do
    conn.host
    |> RoutingTable.get_route()
    |> case do
      nil ->
        Plug.Conn.send_resp(conn, 400, "Bad Request")

      value ->
        %{token | target: value}
    end
  end
end
