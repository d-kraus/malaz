defmodule Malaz.Upstream do
  @moduledoc false

  alias Malaz.Token
  alias Plug.Conn

  def call(%Token{conn: conn, body: body, method: method, target: target}) do
    method
    |> HTTPoison.request(target, body, conn.req_headers, timeout: 5_000)
    |> handle_response(conn)
  end

  defp handle_response({:error, _}, conn), do: Conn.send_resp(conn, 502, "Bad Gateway")

  defp handle_response({:ok, response}, conn) do
    conn
    |> put_resp_headers(response.headers)
    |> Conn.delete_resp_header("transfer-encoding")
    |> Conn.send_resp(response.status_code, response.body)
  end

  defp put_resp_headers(conn, []), do: conn

  defp put_resp_headers(conn, [{header, value} | rest]) do
    conn
    |> Conn.put_resp_header(header |> String.downcase(), value)
    |> put_resp_headers(rest)
  end
end
