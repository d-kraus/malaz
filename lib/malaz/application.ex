defmodule Malaz.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications

  use Application

  def start(_type, _args) do
    children = [
      {Plug.Cowboy,
       scheme: :http, plug: Malaz, port: Application.get_env(:malaz, :cowboy)[:port]},
      Malaz.RoutingTable
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Malaz.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
