defmodule Malaz do
  @moduledoc false

  alias Malaz.Request
  alias Malaz.Router
  alias Malaz.Token
  alias Malaz.Upstream

  def init(_) do
  end

  def call(conn, _opts) do
    %Token{conn: conn}
    |> Router.match()
    |> Request.build()
    |> Upstream.call()
  end
end
