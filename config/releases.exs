import Config

config :malaz, upstreams: System.fetch_env!("UPSTREAMS")

config :malaz, :cowboy, port: System.fetch_env!("PORT") |> String.to_integer()
