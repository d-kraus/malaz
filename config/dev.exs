import Config

config :malaz, :cowboy, port: 80

config :malaz,
  upstreams: "foo.bar -> 127.0.0.1:4044, foo.baz -> 127.0.0.1.4040"
